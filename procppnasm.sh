#!/bin/bash

cp -r ~/Documents/scripts_files/cpproject/ .

PROJECT_PATH="`pwd`/cpproject"
BUILD_PATH="$PROJECT_PATH/build"

# configure project with CMAKE
/usr/bin/cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/x86_64-pc-linux-gnu-gcc-10.2.0 -H/home/vanowikv13/Desktop/cpproject -B/home/vanowikv13/Desktop/cpproject/build -G Ninja

code cpproject/ &
